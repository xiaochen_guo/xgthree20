Pod::Spec.new do |s|
  s.name     = 'XGThree20'
  s.version  = '0.9.9'
  s.license  = 'MIT'
  s.summary  = 'This is trial CocoaPod'
  s.homepage = 'https://bitbucket.org/xiaochen_guo/xgthree20'
  s.authors  = { 'Xiaochen Guo' => 'xiaochen.guo@cantab.net' }
  s.source   = { :git => 'https://xiaochen_guo@bitbucket.org/xiaochen_guo/xgthree20.git'}
  s.requires_arc = false

  s.ios.deployment_target = '6.0'
  s.osx.deployment_target = '10.8'

  s.public_header_files = 'src/**/**/*.h'

  s.subspec 'extThree20JSON' do |ss|
      ss.source_files = 'src/extThree20JSON/**/*.{h,m}'
      ss.frameworks = 'extThree20JSON'
  end

  s.subspec 'extThree20XML' do |ss|
      ss.source_files = 'src/extThree20XML/**/*.{h,m}'
      ss.frameworks = 'extThree20XML'
  end

  s.subspec 'Three20' do |ss|
      ss.source_files = 'src/Three20/**/*.{h,m}'
      ss.frameworks = 'Three20'
  end

  s.subspec 'Three20Core' do |ss|
      ss.source_files = 'src/Three20Core/**/*.{h,m}'
      ss.frameworks = 'Three20Core'
  end

  s.subspec 'Three20Network' do |ss|
      ss.source_files = 'src/Three20Network/**/*.{h,m}'
      ss.frameworks = 'Three20Network'
  end

  s.subspec 'Three20Style' do |ss|
      ss.source_files = 'src/Three20Style/**/*.{h,m}'
      ss.frameworks = 'Three20Style'
  end

  s.subspec 'Three20UI' do |ss|
      ss.source_files = 'src/Three20UI/**/*.{h,m}'
      ss.frameworks = 'Three20UI'
  end

  s.subspec 'Three20UICommon' do |ss|
      ss.source_files = 'src/Three20UICommon/**/*.{h,m}'
      ss.frameworks = 'Three20UICommon'
  end

  s.subspec 'Three20UINavigator' do |ss|
      ss.source_files = 'src/Three20UINavigator/**/*.{h,m}'
      ss.frameworks = 'Three20UINavigator'
  end

end